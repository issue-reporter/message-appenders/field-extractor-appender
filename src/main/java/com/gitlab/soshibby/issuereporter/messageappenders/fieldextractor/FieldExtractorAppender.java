package com.gitlab.soshibby.issuereporter.messageappenders.fieldextractor;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.messageappender.MessageAppender;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FieldExtractorAppender implements MessageAppender {

    private Config configuration;

    @Override
    public void init(String config) {
        configuration = Config.from(config);
        validateConfiguration(configuration);
    }

    @Override
    public String createMessageFrom(LogStatement logTrigger, List<LogStatement> logStatements) {
        for (LogStatement logStatement : logStatements) {
            Map<String, Object> fields = logStatement.getFields();

            if (fields.containsKey(configuration.getFieldName()) && fields.get(configuration.getFieldName()) != null) {
                return fields.get(configuration.getFieldName()).toString();
            }
        }

        return "";
    }

    private void validateConfiguration(Config config) {
        Assert.notNull(config, "Configuration is null.");
        Assert.hasText(config.getFieldName(), "Field name is null or empty in config.");
    }

}

